# ER1172

Objectif des programmes : Ce dossier fournit les programmes permettant de produire les illustrations et les chiffres contenus dans le texte de l'Études et résultats n° 1172 de la DREES : "Perte d’autonomie : à pratiques inchangées, 108 000 seniors de plus seraient attendus en Ehpad d’ici à 2030. Projections de population âgée en perte d’autonomie selon le modèle Lieux de vie et autonomie (LIVIA)".

Lien vers l'étude : https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/publications/etudes-et-resultats/article/perte-d-autonomie-a-pratiques-inchangees-108-000-seniors-de-plus-seraient

Présentation de la DREES : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données : modèle LIVIA (Drees), version au 12/11/2020. Traitements : Drees.

Les programmes ont été exécutés pour la dernière fois avec le logiciel R version 3.6.1, le 12/11/2020.
