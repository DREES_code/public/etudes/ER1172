# # Copyright (C) 2020. Logiciel élaboré par l’État, via la Drees.
# 
# # Nom de l’auteur : Albane Miron de l'Espinay, Drees.
# 
# # Ce programme informatique a été développé par la Drees. 
# 
# # Il permet de produire les tableaux publiés dans l’Études et Résultats  1172, intitulé « Perte d’autonomie : à pratiques inchangées, 108 000 seniors de plus seraient attendus en Ehpad d’ici à 2030. », décembre 2020. 
# 
# # Information sur les versions : Ce programme a été exécuté le 12/11/2020 avec la version 3.6.1 de R et les packages dplyr V.0.8.3, data.table V.1.13.0, rdrees V.0.0.1. 
# 
# # Le texte et les tableaux de l’article peuvent être consultés sur le site de la DREES : https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/publications/etudes-et-resultats/article/perte-d-autonomie-a-pratiques-inchangees-108-000-seniors-de-plus-seraient
# 
# # Ce programme utilise les sorties du modèle LIVIA, dans leur version du 12/11/2020.
# 
# # Bien qu’il n’existe aucune obligation légale à ce sujet, les utilisateurs de ce programme sont invités à signaler à la DREES leurs travaux issus de la réutilisation de ce code, ainsi que les éventuels problèmes ou anomalies qu’ils y rencontreraient, en écrivant à DREES-CODE@sante.gouv.fr
# 
# # Ce logiciel est régi par la licence “GNU General Public License” GPL-3.0. # https://spdx.org/licenses/GPL-3.0.html#licenseText
# 
# # À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
# 
# # Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence GPL-3.0, et que vous en avez accepté les termes.
# 
# # This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# # This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# # You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>. 



library(data.table)
library(rdrees) # Package interne DREES, ne sert qu'à exporter les tableaux au bon template
library(dplyr)

# chemin des données 
chemin_data <- "Data/" # A MODIFIER

# chemin d'enregistrement des résultats
chemin_res <- "Resultats/" # A MODIFIER

# date pour le fichier d'enregistrement
date <- paste0(Sys.Date(), "_")

# Ouverture des données ####
# données totales de sortie de LIVIA : base nationale disponible en Open Data sur data.drees 
# lien : https://data.drees.solidarites-sante.gouv.fr/api/datasets/1.0/projections-du-nombre-de-personnes-agees-en-perte-dautonomie-par-lieu-de-vie-mod/attachments/livia_national_csv/
df <- read.csv(paste0(chemin_data, "livia_national.csv"), sep=";")


# Tableau 1 ####

# ajout de colonnes pour nombre de seniors autonomes en EHPAD
df <- df %>% mutate(vol_n_INS = vol_INS - vol_m_INS - vol_s_INS,
                    vol_n_INS2C = vol_INS2C - vol_m_INS2C - vol_s_INS2C,
                    vol_n_INS2P = vol_INS2P - vol_m_INS2P - vol_s_INS2P)

# Vérification Nombres 2019 pour recalage sur cette année 
df_2019 <- df %>% filter(SEXE==0,moins_75 == 'A', 
                         ANNEE == 2019, scenario == 'central')%>% 
  select_at(vars(contains("vol_")))


# Fonction de génération du tableau 1
tab_comp_hyp <- function(df_, annee_ini, annee_mid, 
                         annee_fin, nom_col, lib_col) {
  
  dt <- data.table(df_)
  cols <- c(nom_col, "hypothese")
  dt_ini <- dt[SEXE==0 & moins_75 == 'A' & 
                ANNEE == annee_ini & scenario == 'central', ..cols]
 
  dt_mid <- dt[SEXE==0 & moins_75 == 'A' & 
                 ANNEE == annee_mid & scenario == 'central', ..cols]
  dt_fin <- dt[SEXE==0 & moins_75 == 'A' & 
                 ANNEE == annee_fin & scenario == 'central', ..cols]
  
  val_ini <- dt[SEXE==0 & moins_75 == 'A' & ANNEE == annee_ini & scenario == 'central' & hypothese == "intermediaire", ..nom_col]
  
  # # Recalage sur l'année 2019 de départ : 610 681 seniors en Ehpad et assimilés en hébergement permanent (panorama AAS)
  if (nom_col == "vol_INS") {val_ini <- dt[SEXE==0 & moins_75 == 'A' & ANNEE == annee_ini & scenario == 'central' & hypothese == "intermediaire", "vol_INS2C"]}
  
  setnames(dt_ini, nom_col, "annee_ini")
  setnames(dt_mid, nom_col, "annee_mid")
  setnames(dt_fin, nom_col, "annee_fin")
  
  dt_ini[, annee_ini := val_ini]
  
  dt_res <- merge(dt_ini, dt_mid, by=c('hypothese'))
  dt_res <- merge(dt_res, dt_fin, by=c('hypothese'))

  dt_res[, ':='("diff_places_midini" = annee_mid - annee_ini, "diff_places_finini" = annee_fin - annee_ini)] 
  
  df_res <- data.frame(dt_res)
  df_res[c(-1)] <- lapply(df_res[c(-1)], function(x) round(x/1000)*1000)
  
  df_res[c(-1)] <- lapply(df_res[c(-1)], function(x) formatC(x,
                                                               big.mark = " ",
                                                               format = "fg"
  ))
  
  
  
  colnames(df_res) <- c("Evolution de la dépendance",
                      paste0(lib_col, " en ", annee_ini),
                      paste0(lib_col, " en ", annee_mid),
                      paste0(lib_col, " en ", annee_fin),
                      paste0("Différence ", annee_ini, "-", annee_mid),
                      paste0("Différence ", annee_ini, "-", annee_fin))
  
  return(df_res)
  
}


sc1 <- tab_comp_hyp(df, 2019, 2030, 2050, "vol_INS", "Nombre de Seniors en EHPAD et assimilés")

exporter_tableau_punaise(sc1, 
                         onglet = 'Institution tableau 1',
                         fichier_destination = paste0(chemin_res, date, "Maq BPC.xlsx"),
                         sources = 'EP24-BHD',
                         champ = "France métropolitaine et DOM (hors Mayotte)",
                         visualiser = F)


sc1 <- tab_comp_hyp(df, 2019, 2030, 2050, "vol_RA", "Nombre de Seniors en résidence autonomie")

exporter_tableau_punaise(sc1, 
                         onglet = 'RA tableau 1',
                         fichier_source = paste0(chemin_res, date, "Maq BPC.xlsx"),
                         sources = 'EP24-BHD',
                         champ = "France métropolitaine et DOM (hors Mayotte)",
                         visualiser = F)


sc1 <- tab_comp_hyp(df, 2019, 2030, 2050, "vol_MENO", "Nombre de Seniors en ménage ordinaire")
exporter_tableau_punaise(sc1, 
                         onglet = 'Ménage tableau 1',
                         fichier_source = paste0(chemin_res, date, "Maq BPC.xlsx"),
                         sources = 'EP24-BHD',
                         champ = "France métropolitaine et DOM (hors Mayotte)",
                         visualiser = F)


# Graphique 2 ####

vol_RA_int <- df %>% filter(scenario=="central" & hypothese == "intermediaire" & moins_75 == "A" & SEXE == 0) %>%
  transmute("Année" = ANNEE,
            "Scénario 1" = vol_RA,
            "Scénario 2" = vol_RA2C,
            "Scénario 3" = vol_RA2P)

exporter_tableau_punaise(vol_RA_int, 
                         fichier_source = paste0(chemin_res, date, "Maq BPC.xlsx"),
                         onglet="Graphique 2",
                         titre = "Nombre de Seniors en résidence autonomie, report de tous les seniors, évolution de la dependance intermédiaire",
                         note = "Scénario 1: pratiques d’entrées en institution inchangées. Scénario 2 : nombre de places en Ehpad constant, au niveau de fin 2019. Scénario 3 : ouverture de nouvelles places en Ehpad au même rythme annuel que celui observé sur la période 2015-2019.
                         L’hypothèse intermédiaire d’évolution de la dépendance correspond à une part de l’espérance de vie en perte d’autonomie modérée (GIR 3-4) dans l’espérance de vie totale restant constante à l’avenir.",
                         lecture = "En 2030, si les seniors autonomes et dépendants sont accueillis en résidence autonomie, selon le scénario 1 d’ouverture de places en Ehpad, 140 000 vivront en résidence autonomie. Selon le scénario 2, ils seraient 250 000, et 200 000 selon le scénario 3.",
                         champ = "France, hors Mayotte",
                         sources = "DREES, modèle LIVIA",
                         visualiser = FALSE)

# Tableau complémentaire A ####

vol_RA_m_int <- df %>% filter(scenario=="central" & hypothese == "intermediaire" & moins_75 == "A" & SEXE == 0) %>%
  transmute("Année" = ANNEE,
            "Scénario 1" = vol_m_RA,
            "Scénario 2" = vol_m_RA2C,
            "Scénario 3" = vol_m_RA2P)

log_2_int <- 
  data.frame("Année" = vol_RA_int$Année, 
             "Scénario 1" = vol_RA_int$`Scénario 1` + (vol_RA_m_int$`Scénario 1` - vol_RA_m_int$`Scénario 1`),
             "Scénario 2" = vol_RA_int$`Scénario 1` + (vol_RA_m_int$`Scénario 2` - vol_RA_m_int$`Scénario 1`),
             "Scénario 3" = vol_RA_int$`Scénario 1` + (vol_RA_m_int$`Scénario 3` - vol_RA_m_int$`Scénario 1`))


exporter_tableau_punaise(log_2_int, 
                         fichier_source = paste0(chemin_res, date, "Maq BPC.xlsx"),
                         onglet="Tableau complémentaire A",
                         visualiser = FALSE,
                         titre = "Nombre de Seniors en résidence autonomie, report des seniors dépendants uniquement, évolution de la dependance intermédiaire",
                         note = "Scénario 1: pratiques d’entrées en institution inchangées. Scénario 2 : nombre de places en Ehpad constant, au niveau de fin 2019. Scénario 3 : ouverture de nouvelles places en Ehpad au même rythme annuel que celui observé sur la période 2015-2019.
                         L’hypothèse intermédiaire d’évolution de la dépendance correspond à une part de l’espérance de vie en perte d’autonomie modérée (GIR 3-4) dans l’espérance de vie totale restant constante à l’avenir.",
                         lecture = "En 2030, si les seniors autonomes et dépendants sont accueillis en résidence autonomie, selon le scénario 1 d’ouverture de places en Ehpad, 140 000 vivront en résidence autonomie. Selon le scénario 2, ils seraient 250 000, et 200 000 selon le scénario 3.",
                         champ = "France, hors Mayotte",
                         sources = "DREES, modèle LIVIA")


# Graphique 2, Graphique 3 et Tableau complémentaire B ####

gen_col <- function(df, an){
  df_int <- df %>% filter(scenario == "central", hypothese == "intermediaire",
                          ANNEE == an, SEXE == 0, moins_75 == "A")
  df_opt <- df %>% filter(scenario == "central", hypothese == "optimiste",
                          ANNEE == an, SEXE == 0, moins_75 == "A")
  
  df_pes <- df %>% filter(scenario == "central", hypothese == "pessimiste",
                          ANNEE == an, SEXE == 0, moins_75 == "A")
  
  res1 <- c(df_int$vol_s_INS, df_int$vol_m_INS, df_int$vol_n_INS,
           df_opt$vol_s_INS, df_opt$vol_m_INS, df_opt$vol_n_INS,
           df_pes$vol_s_INS, df_pes$vol_m_INS, df_pes$vol_n_INS)
  
  res2 <- c(df_int$vol_s_INS2C, df_int$vol_m_INS2C, df_int$vol_n_INS2C,
           df_opt$vol_s_INS2C, df_opt$vol_m_INS2C, df_opt$vol_n_INS2C,
           df_pes$vol_s_INS2C, df_pes$vol_m_INS2C, df_pes$vol_n_INS2C)
  
  res3 <- c(df_int$vol_s_INS2P, df_int$vol_m_INS2P, df_int$vol_n_INS2P,
           df_opt$vol_s_INS2P, df_opt$vol_m_INS2P, df_opt$vol_n_INS2P,
           df_pes$vol_s_INS2P, df_pes$vol_m_INS2P, df_pes$vol_n_INS2P)
  
  res <- list(res1, res2, res3)
  return(res)
}

r15 <- gen_col(df, 2019)
r30 <- gen_col(df, 2030)
r50 <- gen_col(df, 2050)

# Recalage sur 2019 : scénario 2 stabilité des places et évolution intermédiaire de la dépendance
recal_gir <- rep(r15[[2]][c(1, 2, 3)], 3)

df_res <- data.frame("Niveau de dépendance" = rep(c("Sévèrement dépendants", "Modérément dépendants", "Autonomes"), 3),
  "Départ de la projection : 2019" = recal_gir,
                     "2030 scénario 1" = r30[[1]],
                     "2030 scénario 2" = r30[[2]],
                     "2030 scénario 3" = r30[[3]],
                     "2050 scénario 1" = r50[[1]],
                     "2050 scénario 2" = r50[[2]],
                     "2050 scénario 3" = r50[[3]])


df_res_int <- df_res[1:3,]
df_res_opt <- df_res[4:6,]
df_res_pes <- df_res[7:9,]

exporter_tableau_punaise(df_res_int,
                         fichier_source = paste0(chemin_res, date, "Maq BPC.xlsx"),
                         onglet="Graphique 1",
                         visualiser = FALSE,
                         titre = "Nombre projeté de résidents en Ehpad par niveau de dépendance en 2019, 2030, 2050 selon la politique de maintien à domicile. 
Hypothèse intermédiaire de l'évolution de la dépendance",
                         champ = "France, hors Mayotte",
                         sources = "DREES, modèle LIVIA")

exporter_tableau_punaise(df_res_opt,
                         fichier_source = paste0(chemin_res, date, "Maq BPC.xlsx"),
                         onglet="Graphique 3",
                         visualiser = FALSE,
                         titre = "Nombre projeté de résidents en Ehpad par niveau de dépendance en 2019, 2030, 2050 selon la politique de maintien à domicile. 
Hypothèse optimiste de l'évolution de la dépendance
",
                         champ = "France, hors Mayotte",
                         sources = "DREES, modèle LIVIA")

exporter_tableau_punaise(df_res_pes,
                         fichier_source = paste0(chemin_res, date, "Maq BPC.xlsx"),
                         onglet="Tableau complémentaire B",
                         visualiser = FALSE,
                         titre = "Nombre projeté de résidents en Ehpad par niveau de dépendance en 2019, 2030, 2050 selon la politique de maintien à domicile.
                         Hypothèse pessimiste de l'évolution de la dépendance",
                         champ = "France, hors Mayotte",
                         sources = "DREES, modèle LIVIA")


# Ajout répartition des GIR dans les lieux de vie selon les scénarios : chiffres dans le corps du texte de la publication ####

repart_gir <- function(df_, str_col, annee){
  
  df_ <- data.table(df_)
  dfan <- df_[ANNEE == annee & SEXE == 0& moins_75 == "A"& scenario == "central"& hypothese == "intermediaire"] 

  colgir12 <-  paste0("vol_s_", str_col)
  colgir34 <- paste0("vol_m_", str_col)
  coltous <- paste0("vol_", str_col)
  
  tauxgir12 <- as.numeric(dfan[1, ..colgir12]) / as.numeric(dfan[1, ..coltous])
  tauxgir34 <- as.numeric(dfan[1, ..colgir34]) / as.numeric(dfan[1, ..coltous])
  tauxgir56 <- 1 - tauxgir12 - tauxgir34

  return(list(gir12 = tauxgir12, gir34 = tauxgir34, gir56 = tauxgir56))
  
}

repart_gir(df, "RA", 2015)

# avec report total de la population sans place en EHPAD en RA
repart_gir(df, "RA", 2030) # sc 1
repart_gir(df, "RA2C", 2030) # sc 3
repart_gir(df, "RA2P", 2030) # sc 3

# en EHPAD 
repart_gir(df, "INS", 2015)

repart_gir(df, "INS", 2019)


repart_gir(df, "INS", 2030) # sc 1
repart_gir(df, "INS2C", 2030) # sc 2
repart_gir(df, "INS2P", 2030) # sc 3

# 2050
repart_gir(df, "INS", 2050) # sc 1
repart_gir(df, "INS2C", 2050) # sc 2
repart_gir(df, "INS2P", 2050) # sc 3
repart_gir(df, "INS2P", 2035) # sc 3


# avec report uniquement de la population dépendante sans places en EHPAD
df <- data.table(df)

df[, vol_RA2C_part := vol_RA + ajout_m_RA2C + ajout_s_RA2C]
df[, vol_m_RA2C_part := vol_m_RA + ajout_m_RA2C]
df[, vol_s_RA2C_part := vol_s_RA + ajout_s_RA2C]
df[, vol_RA2P_part := vol_RA + ajout_m_RA2P + ajout_s_RA2P]
df[, vol_m_RA2P_part := vol_m_RA + ajout_m_RA2P]
df[, vol_s_RA2P_part := vol_s_RA + ajout_s_RA2P]

repart_gir(df, "RA", 2015)

# avec report uniquement de la population dépendante sans places en EHPAD
repart_gir(df, "RA", 2030) # sc 1
repart_gir(df, "RA2C_part", 2030) # sc 3
repart_gir(df, "RA2P_part", 2030) # sc 3

